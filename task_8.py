import turtle

class Drow:
	def __init__(self, color, lenght):
		self.color = color
		self.lenght = lenght


	def triangle(self):
		turtle.color(self.color)
		for i in range(3):
			turtle.forward(self.lenght)
			turtle.left(120)

	def square(self):
		for i in range(4):
			turtle.forward(self.lenght)
			turtle.left(90)
		
	def tab(self):
		turtle.up()
		turtle.forward(190)
		turtle.down()

	def circle(self):
		turtle.circle(self.lenght / 2)


	def star(self):
		turtle.left(72)
		for i in range(5):
			turtle.forward(self.lenght)
			turtle.right(144)
		turtle.right(72)

	def drow_all(self):
		turtle.begin_fill()
		r.triangle()
		r.tab()
		r.square()
		r.tab()
		r.circle()
		r.tab()
		r.star()
		turtle.end_fill()



r = Drow('blue', 100)	

r.drow_all()

turtle.exitonclick()
